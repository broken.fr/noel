<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201017224550 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Log (log_id INT AUTO_INCREMENT NOT NULL COMMENT \'ID .\', fk_gift_id INT DEFAULT NULL COMMENT \'ID .\', fk_santa_user_id INT DEFAULT NULL, log_type VARCHAR(255) NOT NULL COMMENT \'Type de log, add or select\', log_date DATETIME NOT NULL COMMENT \'Date\', INDEX IDX_B7722E25678667AF (fk_gift_id), INDEX IDX_B7722E25DFEBE3D7 (fk_santa_user_id), PRIMARY KEY(log_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift (gift_id INT AUTO_INCREMENT NOT NULL COMMENT \'ID .\', fk_giftlist_id INT DEFAULT NULL COMMENT \'ID de la giftliste.\', fk_user_id INT DEFAULT NULL, gift_selected TINYINT(1) NOT NULL COMMENT \'selectionné?\', gift_surprised TINYINT(1) NOT NULL COMMENT \'surprise?\', gift_link VARCHAR(255) DEFAULT NULL COMMENT \'Lien du cadeau\', gift_link_bis VARCHAR(255) DEFAULT NULL COMMENT \'Lien du cadeau bis\', gift_name VARCHAR(255) NOT NULL COMMENT \'nom du cadeau\', gift_description VARCHAR(255) DEFAULT NULL COMMENT \'description du cadeau\', INDEX IDX_A47C990DF1CD1F68 (fk_giftlist_id), INDEX IDX_A47C990D5741EEB9 (fk_user_id), PRIMARY KEY(gift_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift_join_santa (gift_id INT NOT NULL COMMENT \'ID .\', user_id INT NOT NULL, INDEX IDX_ECD6423497A95A83 (gift_id), INDEX IDX_ECD64234A76ED395 (user_id), PRIMARY KEY(gift_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE giftlist (giftlist_id INT AUTO_INCREMENT NOT NULL COMMENT \'ID de la giftliste.\', fk_santa_user_id INT NOT NULL, giftlist_name VARCHAR(255) NOT NULL COMMENT \'nom de la giftliste\', giftlist_date DATE NOT NULL COMMENT \'Date\', INDEX IDX_1651F2B1DFEBE3D7 (fk_santa_user_id), PRIMARY KEY(giftlist_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE santa_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', icon VARCHAR(255) DEFAULT \'fas fa-hat-santa\' NOT NULL COMMENT \'icon utilisateur\', icon_color VARCHAR(255) DEFAULT \'red-text\' NOT NULL COMMENT \'couleur icon utilisateur\', UNIQUE INDEX UNIQ_BD96080B92FC23A8 (username_canonical), UNIQUE INDEX UNIQ_BD96080BA0D96FBF (email_canonical), UNIQUE INDEX UNIQ_BD96080BC05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Log ADD CONSTRAINT FK_B7722E25678667AF FOREIGN KEY (fk_gift_id) REFERENCES gift (gift_id)');
        $this->addSql('ALTER TABLE Log ADD CONSTRAINT FK_B7722E25DFEBE3D7 FOREIGN KEY (fk_santa_user_id) REFERENCES santa_user (id)');
        $this->addSql('ALTER TABLE gift ADD CONSTRAINT FK_A47C990DF1CD1F68 FOREIGN KEY (fk_giftlist_id) REFERENCES giftlist (giftlist_id)');
        $this->addSql('ALTER TABLE gift ADD CONSTRAINT FK_A47C990D5741EEB9 FOREIGN KEY (fk_user_id) REFERENCES santa_user (id)');
        $this->addSql('ALTER TABLE gift_join_santa ADD CONSTRAINT FK_ECD6423497A95A83 FOREIGN KEY (gift_id) REFERENCES gift (gift_id)');
        $this->addSql('ALTER TABLE gift_join_santa ADD CONSTRAINT FK_ECD64234A76ED395 FOREIGN KEY (user_id) REFERENCES santa_user (id)');
        $this->addSql('ALTER TABLE giftlist ADD CONSTRAINT FK_1651F2B1DFEBE3D7 FOREIGN KEY (fk_santa_user_id) REFERENCES santa_user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Log DROP FOREIGN KEY FK_B7722E25678667AF');
        $this->addSql('ALTER TABLE gift_join_santa DROP FOREIGN KEY FK_ECD6423497A95A83');
        $this->addSql('ALTER TABLE gift DROP FOREIGN KEY FK_A47C990DF1CD1F68');
        $this->addSql('ALTER TABLE Log DROP FOREIGN KEY FK_B7722E25DFEBE3D7');
        $this->addSql('ALTER TABLE gift DROP FOREIGN KEY FK_A47C990D5741EEB9');
        $this->addSql('ALTER TABLE gift_join_santa DROP FOREIGN KEY FK_ECD64234A76ED395');
        $this->addSql('ALTER TABLE giftlist DROP FOREIGN KEY FK_1651F2B1DFEBE3D7');
        $this->addSql('DROP TABLE Log');
        $this->addSql('DROP TABLE gift');
        $this->addSql('DROP TABLE gift_join_santa');
        $this->addSql('DROP TABLE giftlist');
        $this->addSql('DROP TABLE santa_user');
    }
}
