<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201017233538 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_BD96080BC05FB297 ON santa_user');
        $this->addSql('DROP INDEX UNIQ_BD96080B92FC23A8 ON santa_user');
        $this->addSql('DROP INDEX UNIQ_BD96080BA0D96FBF ON santa_user');
        $this->addSql('ALTER TABLE santa_user DROP username_canonical, DROP email_canonical, DROP enabled, DROP last_login, DROP confirmation_token, DROP password_requested_at, CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BD96080BE7927C74 ON santa_user (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BD96080BF85E0677 ON santa_user (username)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_BD96080BE7927C74 ON santa_user');
        $this->addSql('DROP INDEX UNIQ_BD96080BF85E0677 ON santa_user');
        $this->addSql('ALTER TABLE santa_user ADD username_canonical VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD email_canonical VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD enabled TINYINT(1) NOT NULL, ADD last_login DATETIME DEFAULT NULL, ADD confirmation_token VARCHAR(180) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD password_requested_at DATETIME DEFAULT NULL, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BD96080BC05FB297 ON santa_user (confirmation_token)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BD96080B92FC23A8 ON santa_user (username_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BD96080BA0D96FBF ON santa_user (email_canonical)');
    }
}
