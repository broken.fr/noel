<?php

namespace App\Controller;

use App\Entity\Log;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        return $this->forward(ListManagmentController::class . ':listSelectAction');
    }

    /**
     * @Route("/updates", name="updates")
     */
    public function UpdatesAction(Request $request)
    {
        $htmlTitle = 'Actions';

        $entityManager = $this->getDoctrine()->getManager();
        $logRepository = $entityManager->getRepository(Log::class);

        $logs = $logRepository->findBy([], ['date' => 'desc', 'gift' => 'asc'], 30);

        return $this->render('default/index.html.twig', [
            'log_title' => $htmlTitle,
            'logs' => $logs
        ]);
    }
}
