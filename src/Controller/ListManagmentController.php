<?php

namespace App\Controller;

use App\Entity\Log;
use App\Entity\Gift;
use App\Entity\GiftList;
use App\Entity\SantaUser as User;
use App\Form\GiftType;
use App\Form\GiftListType;
use App\Form\GiftsListType;


use App\Form\SurpriseGiftType;
use Doctrine\ORM\Repository\RepositoryFactory;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ListManagmentController extends AbstractController
{

    /**
     * @Route("/gift/edit", name="gift_edit")
     */
    public function giftEditAction(Request $request)
    {
        $htmlTitle = 'Nouveau Cadeau surprise';
        $htmlDesc = 'Pour la surprise, il est conseillé de laisser le nom par défaut. <br/>La personne ne vera pas le lien du cadeau ;)';
        $gift = new Gift();
        $gift->setName('Cadeau surprise!');
        $form = $this->createForm(SurpriseGiftType::class, $gift);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $listRepository = $entityManager->getRepository(GiftList::class);
            $year = new \DateTime();
            $year->setDate($year->format('Y'), 1, 1);
            $list = $listRepository->findOneBy(['user' => $gift->getUser(), 'date' => $year]);
            if(!$list){
                $list = new GiftList();
                $list->setName('liste ' . $gift->getUser()->getUsername());
                $list->setDate($year);
                $list->setUser($gift->getUser());
            }
            // $form->getData() holds the submitted values
            $gift = $form->getData();
            $gift->setSurprised(true);
            $gift->setUser($gift->getUser());
            $list->addGift($gift);
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager->persist($list);
            $entityManager->persist($gift);
            $entityManager->flush();

            return $this->redirectToRoute('list_select');
        }

        return $this->render('gift/edit.html.twig', [
            'form' => $form->createView(),
            'gift_edit_title' => $htmlTitle,
            'gift_edit_desc' => $htmlDesc,
        ]);
    }

    /**
     * @Route("/list/edit", name="list_edit")
     */
    public function listEditAction(Request $request)
    {
        $htmlTitle = 'Ma Liste';
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $listRepository = $entityManager->getRepository(GiftList::class);
        $logRepository = $entityManager->getRepository(Log::class);

        $year = new \DateTime();
        $year->setDate($year->format('Y'), 1, 1);
        $list = $listRepository->findOneBy(['user' => $user, 'date' => $year]);
        if(!$list){
            $htmlTitle = 'Créer une Liste';
            $list = new GiftList();
        }
        $gifts = clone $list->getGifts();

        $form = $this->createForm(GiftListType::class, $list);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            $list = $form->getData();
            $giftsColl = $list->getGifts();
            foreach($giftsColl as $gift){
                $gift->setUser($user);
                $exist = $logRepository->findOneBy(['gift' => $gift, 'santa' => $user, 'type' => Log::ADD]);
                if(!$exist){
                    $log = new Log();
                    $log->setType(Log::ADD);
                    $log->setGift($gift);
                    $log->setSanta($user);
                    $log->setDate(new \DateTime());
                    $entityManager->persist($log);
                }
            }
            foreach ($gifts as $giftToRemove) {
                if(!$giftToRemove->getSurprised() && !$giftsColl->contains($giftToRemove)){
                    $giftsColl->removeElement($giftToRemove);
                    $entityManager->remove($giftToRemove);
                }
            }
            $list->setUser($user);
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager->persist($list);
            $entityManager->flush();

            return $this->redirectToRoute('list_edit');
        }

        return $this->render('list/edit.html.twig', [
            'form' => $form->createView(),
            'list_edit_title' => $htmlTitle
        ]);
    }

    /**
     * @Route("/list/select", name="list_select")
     */
    public function listSelectAction(Request $request)
    {
        $htmlTitle = 'Listes des Cadeaux';

        $entityManager = $this->getDoctrine()->getManager();
        $listRepository = $entityManager->getRepository(GiftList::class);
        $year = new \DateTime();
        $year->setDate($year->format('Y'), 1, 1);
        $lists = $listRepository->findBy(['date' => $year]);

        $forms = [];
        $i = 0;
        foreach ($lists as $list) {
            if(!$list->getGifts()->isEmpty()){
                $form = $this->createForm(GiftsListType::class, $list, ["block_name" => "gifts_list_" . $i]);

                $forms[] = $form->createView();
                $i++;
            }
        }

        return $this->render('list/index.html.twig', [
            'list_select_title' => $htmlTitle,
            'forms' => $forms,
            'user_id' => $this->getUser()->getId()
        ]);
    }

    /**
     * @Route("/list/gift/select", options={"expose"=true}, name="list_gift_select")
     */
    public function listSelectGiftAction(Request $request)
    {
        $status = 'error';
        $message = '';
        if(!$this->getUser()){
            throw new \Exception("Error Processing Request, Must be logged", 1);
        }
        if($request->isXmlHttpRequest()){
            $post = $request->request->all();
            $message = 'Invalid given data';
            if(isset($post['gift']) && isset($post['list'])){
                $entityManager = $this->getDoctrine()->getManager();
                $gift = $entityManager->getRepository(Gift::class)->findOneBy([
                    'id' => $post['gift'],
                    'giftlist' => $post['list']
                ]);
                if($gift){
                    if($gift->getSantas()->contains($this->getUser())){
                        $gift->removeSanta($this->getUser());
                        $entityManager->persist($gift);
                        $log = $entityManager->getRepository(Log::class)->findOneBy([
                            'type' => Log::SELECT,
                            'gift' => $gift,
                            'santa' => $this->getUser()
                        ]);
                        if($log) $entityManager->remove($log);
                        try {
                            $entityManager->flush();
                            $status = 'info';
                            $message = 'Cadeau décoché !';
                        } catch (\Exception $e) {
                            $message = $e->getMessage();
                        }
                    } else {
                        // $gift->setSelected(true);
                        $gift->addSanta($this->getUser());
                        $log = new Log();
                        $log->setType(Log::SELECT);
                        $log->setGift($gift);
                        $log->setSanta($this->getUser());
                        $log->setDate(new \DateTime());
                        $entityManager->persist($log);
                        $entityManager->persist($gift);
                        try {
                            $entityManager->flush();
                            $status = 'success';
                            $message = 'Cadeau coché !';
                        } catch (\Exception $e) {
                            $message = $e->getMessage();
                        }
                    }
                }
            }
        } else {
            $message = 'Invalid Request : Not Xhr';
        }

        $response = array(
            'status' => $status,
            'message' => $message
        );
        return new JsonResponse($response);
    }

    /**
     * @Route("/list/gift/selected", name="list_gift_selected")
     */
    public function listSelectedGiftsAction(Request $request)
    {
        $htmlTitle = 'Cadeaux Sélectionnés';
        $id = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $year = (new \DateTime())->format('Y');
        $giftRepository = $entityManager->getRepository(Gift::class);

        $giftsToProcess = $giftRepository->findAll();

        $gifts = [];
        /** @var Gift $gift */
        foreach ($giftsToProcess as $gift) {
            $giftId = $gift->getId();
            $giftDate = $gift->getGiftList()->getDate()->format('Y');
            if ($giftDate === $year) {
                foreach($gift->getSantas() as $santa){
                    if($santa->getId() === $id){
                        $gifts[$giftId]['gift'] = $gift;
                    } else {
                        $gifts[$giftId]['users'][] = $santa;
                    }
                }
            }
        }
        return $this->render('list/selected.html.twig', [
            'selected_title' => $htmlTitle,
            'gifts' => $gifts
        ]);
    }
}
