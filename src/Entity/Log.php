<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Log
 *
 * @ORM\Table( name="Log" )
 * @ORM\Entity
 */
class Log
{
    const ADD = "add";
    const SELECT = "select";
    const TYPE = [
        self::ADD, self::SELECT
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="log_id", type="integer", nullable=false, options={"comment"="ID ."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Gift
     *
     * @ORM\ManyToOne(targetEntity="Gift", inversedBy="logs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_gift_id", referencedColumnName="gift_id", nullable=true)
     * })
     */
    private $gift;

    /**
     * @var SantaUser
     *
     * @ORM\ManyToOne(targetEntity="SantaUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_santa_user_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $santa;

    /**
     * @var string
     *
     * @ORM\Column(name="log_type", type="string", length=255, nullable=false, options={"comment"="Type de log, add or select"})
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="log_date", type="datetime", nullable=false, options={"comment"="Date"})
     */
    private $date;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Log
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Log
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set gift.
     *
     * @param Gift|null $gift
     *
     * @return Log
     */
    public function setGift(Gift $gift = null)
    {
        $this->gift = $gift;

        return $this;
    }

    /**
     * Get gift.
     *
     * @return Gift|null
     */
    public function getGift()
    {
        return $this->gift;
    }

    /**
     * Set santa.
     *
     * @param SantaUser|null $santa
     *
     * @return Log
     */
    public function setSanta(SantaUser $santa = null)
    {
        $this->santa = $santa;

        return $this;
    }

    /**
     * Get santa.
     *
     * @return SantaUser|null
     */
    public function getSanta()
    {
        return $this->santa;
    }
}
