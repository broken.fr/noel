<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gift
 *
 * @ORM\Table( name="gift" )
 * @ORM\Entity
 */
class Gift
{

    /**
     * @var integer
     *
     * @ORM\Column(name="gift_id", type="integer", nullable=false, options={"comment"="ID ."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var GiftList
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\GiftList", inversedBy="gifts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_giftlist_id", referencedColumnName="giftlist_id")
     * })
     */
    private $giftlist;

    /**
     * @var SantaUser
     *
     * @ORM\ManyToOne(targetEntity="SantaUser", inversedBy="gifts", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $user;

    /**
     * @var SantaUser
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\SantaUser", fetch="EAGER")
     * @ORM\JoinTable(name="gift_join_santa",
     *   joinColumns={
     *     @ORM\JoinColumn(name="gift_id", referencedColumnName="gift_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *   }
     * )
     */
    private $santas;

    /**
     * @var ArrayCollection $logs
     *
     * @ORM\OneToMany(targetEntity="Log", mappedBy="gift", cascade={"remove"})
     */
    private $logs;

    /**
     * @var boolean
     *
     * @ORM\Column(name="gift_selected", type="boolean", nullable=false, options={"comment"="selectionné?"})
     */
    private $selected = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="gift_surprised", type="boolean", nullable=false, options={"comment"="surprise?"})
     */
    private $surprised = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gift_link", type="text", nullable=true, options={"comment"="Lien du cadeau"})
     */
    private $link;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gift_link_bis", type="text", nullable=true, options={"comment"="Lien du cadeau bis"})
     */
    private $linkBis;

    /**
     * @var string
     *
     * @ORM\Column(name="gift_name", type="string", length=255, nullable=false, options={"comment"="nom du cadeau"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="gift_description", type="string", length=255, nullable=true, options={"comment"="description du cadeau"})
     */
    private $description;

    public function __construct() {
        $this->logs = new ArrayCollection();
        $this->santas = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set selected
     *
     * @param boolean $selected
     *
     * @return Gift
     */
    public function setSelected(bool $selected)
    {
        $this->selected = $selected;

        return $this;
    }

    /**
     * Get selected
     *
     * @return boolean
     */
    public function getSelected()
    {
        return $this->selected;
    }

    /**
     * Set surprised
     *
     * @param boolean $surprised
     *
     * @return Gift
     */
    public function setSurprised(bool $surprised)
    {
        $this->surprised = $surprised;

        return $this;
    }

    /**
     * Get surprised
     *
     * @return boolean
     */
    public function getSurprised()
    {
        return $this->surprised;
    }

    /**
     * Set link
     *
     * @param string|null $link
     *
     * @return Gift
     */
    public function setLink(?string $link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string|null
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set linkBis
     *
     * @param string|null $linkBis
     *
     * @return Gift
     */
    public function setLinkBis(?string $linkBis)
    {
        $this->linkBis = $linkBis;

        return $this;
    }

    /**
     * Get linkBis
     *
     * @return null|string
     */
    public function getLinkBis()
    {
        return $this->linkBis;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Gift
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set giftlist
     *
     * @param GiftList|null $giftlist
     *
     * @return $this
     */
    public function setGiftList(GiftList $giftlist = null)
    {
        $this->giftlist = $giftlist;

        return $this;
    }

    /**
     * Get giftlist
     *
     * @return GiftList
     */
    public function getGiftList()
    {
        return $this->giftlist;
    }

    /**
     * Set user.
     *
     * @param SantaUser|null $user
     *
     * @return Gift
     */
    public function setUser(SantaUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return SantaUser|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Clear Santa collection.
     *
     * @return Gift
     */
    public function clearSanta()
    {
        $this->santas->clear();

        return $this;
    }

    /**
     * Add Santa.
     *
     * @param SantaUser $santa
     *
     * @return Gift
     */
    public function addSanta(SantaUser $santa)
    {
        $this->santas[] = $santa;

        return $this;
    }

    /**
     * Remove Santa.
     *
     * @param SantaUser $santa
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSanta(SantaUser $santa)
    {
        return $this->santas->removeElement($santa);
    }

    /**
     * Get Santas.
     *
     * @return Collection
     */
    public function getSantas()
    {
        return $this->santas;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     *
     * @return Gift
     */
    public function setDescription(?string $description): Gift
    {
        $this->description = $description;
        return $this;
    }

}
