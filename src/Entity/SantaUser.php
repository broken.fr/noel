<?php

namespace App\Entity;

use App\Repository\SantaUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
//use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * SantaUser
 *
 * @ORM\Table(name="santa_user")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=SantaUserRepository::class)
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class SantaUser implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=false, options={"comment"="icon utilisateur", "default"="fas fa-hat-santa"})
     */
    private $icon = 'fas fa-hat-santa';

    /**
     * @var string
     *
     * @ORM\Column(name="icon_color", type="string", length=255, nullable=false, options={"comment"="couleur icon utilisateur", "default"="red-text"})
     */
    private $iconColor = 'red-text';

    /**
     * @var ArrayCollection $gifts
     *
     * @ORM\OneToMany(targetEntity="Gift", mappedBy="user")
     * @ORM\OrderBy({"name" = "asc"})
     */
    private $gifts;

    /**
     * @var ArrayCollection $giftlists
     *
     * @ORM\OneToMany(targetEntity="GiftList", mappedBy="user")
     * @ORM\OrderBy({"date" = "asc"})
     */
    private $giftlists;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * SantaUser constructor.
     */
    public function __construct()
    {
        $this->gifts = new ArrayCollection();
        $this->giftlists = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return SantaUser
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return SantaUser
     */
    public function setIcon(string $icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set iconColor
     *
     * @param string $iconColor
     *
     * @return SantaUser
     */
    public function setIconColor(string $iconColor)
    {
        $this->iconColor = $iconColor;

        return $this;
    }

    /**
     * Get iconColor
     *
     * @return string
     */
    public function getIconColor()
    {
        return $this->iconColor;
    }


    /**
     * Add giftlist.
     *
     * @param GiftList $giftlist
     *
     * @return SantaUser
     */
    public function addGiftlist(GiftList $giftlist)
    {
        $this->giftlists[] = $giftlist;

        return $this;
    }

    /**
     * Remove giftlist.
     *
     * @param GiftList $giftlist
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGiftlist(GiftList $giftlist)
    {
        return $this->giftlists->removeElement($giftlist);
    }

    /**
     * Get giftlists.
     *
     * @return Collection
     */
    public function getGiftlists()
    {
        return $this->giftlists;
    }

    /**
     * Add gift.
     *
     * @param Gift $gift
     *
     * @return SantaUser
     */
    public function addGift(Gift $gift)
    {
        $this->gifts[] = $gift;

        return $this;
    }

    /**
     * Remove gift.
     *
     * @param Gift $gift
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGift(Gift $gift)
    {
        return $this->gifts->removeElement($gift);
    }

    /**
     * Get gifts.
     *
     * @return Collection
     */
    public function getGifts()
    {
        return $this->gifts;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getSalt()
    {
        return null;
    }
}
