<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * GiftList
 *
 * @ORM\Table(  name="giftlist" )
 * @ORM\Entity
 */
class GiftList
{

    /**
     * @var integer
     *
     * @ORM\Column(name="giftlist_id", type="integer", nullable=false, options={"comment"="ID de la giftliste."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ArrayCollection $gifts
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Gift", mappedBy="giftlist", cascade={"persist"}, fetch="EAGER")
     * @ORM\OrderBy({"name" = "asc"})
     */
    private $gifts;

    /**
     * @var SantaUser
     *
     * @ORM\ManyToOne(targetEntity="SantaUser", inversedBy="giftlists", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_santa_user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="giftlist_name", type="string", length=255, nullable=false, options={"comment"="nom de la giftliste"})
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="giftlist_date", type="date", nullable=false, options={"comment"="Date"})
     */
    private $date;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gifts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GiftList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return GiftList
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Add gift
     *
     * @param Gift $gift
     *
     * @return GiftList
     */
    public function addGift(Gift $gift)
    {
        // for a many-to-one association:
        $gift->setGiftList($this);
        $this->gifts->add($gift);

        return $this;
    }

    /**
     * Remove gift
     *
     * @param \App\Entity\Gift $gift
     */
    public function removeGift(\App\Entity\Gift $gift)
    {
        $this->gifts->removeElement($gift);
    }

    /**
     * Get gifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGifts()
    {
        return $this->gifts;
    }

    /**
     * Get gifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUnsuprisedGifts()
    {
        $gifts = new \Doctrine\Common\Collections\ArrayCollection();
        foreach($this->gifts as $gift){
            if(!$gift->getSurprised()){
                $gifts->add($gift);
            }
        }

        return $gifts;
    }

    /**
     * Set user.
     *
     * @param \App\Entity\SantaUser|null $user
     *
     * @return GiftList
     */
    public function setUser(\App\Entity\SantaUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \App\Entity\SantaUser|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
