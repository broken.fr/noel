<?php

namespace App\Form;

use App\Entity\GiftList;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class GiftListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $list = $builder->getData();
        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'list.name',
                    'required' => true
                )
            )
            ->add(
                'date',
                DateType::class,
                array(
                    'widget' => 'single_text',
                    'format' => 'yyyy',
                    'html5' => false,
                    'label' => 'list.date',
                    'required' => true,
                    'data' => new \DateTime()
                )
            )
            ->add(
                'gifts',
                CollectionType::class,
                array(
                    'label' => 'list.gifts',
                    'entry_type' => GiftType::class,
                    'entry_options' => array('label' => false),
                    "allow_delete" => true,
                    "allow_add" => true,
                    "delete_empty" => true,
                    'by_reference' => false,
                    'data' => $list->getUnsuprisedGifts()
                )
            )
            ->add(
                'create',
                SubmitType::class,
                array(
                    'attr' => ['class' => 'aqua-gradient m-0'],
                    'label' => 'submit.create'
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GiftList::class,
            'translation_domain' => 'form',
        ));
    }
}
