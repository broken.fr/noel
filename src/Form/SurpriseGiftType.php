<?php

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use App\Entity\Gift;

use App\Entity\SantaUser;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SurpriseGiftType extends AbstractType
{
    protected $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $builder
            ->add('user', EntityType::class, [
                'class' => SantaUser::class,
                'label' => 'user.name',
                'required' => true,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.username', 'ASC')
                        ->where('u != :user')
                        ->setParameter('user', $user)
                        ;
                },
                'choice_label' => 'username',
            ])
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'gift.name',
                    'required' => true
                )
            )
            ->add(
                'description',
                TextType::class,
                array(
                    'label' => 'gift.description',
                    'required' => false
                )
            )
            ->add(
                'link',
                TextType::class,
                array(
                    'label' => 'gift.link',
                    'required' => false
                )
            )
            ->add(
                'linkBis',
                TextType::class,
                array(
                    'label' => 'gift.linkBis',
                    'required' => false
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Gift::class,
            'translation_domain' => 'form',
        ));
    }
}
