<?php

namespace App\Form;

use App\Entity\GiftList;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class GiftsListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAttribute('name', $options['block_name']);
        $list = $builder->getData();
        $builder
            ->add(
                'gifts',
                CollectionType::class,
                array(
                    'label' => 'list.gifts',
                    'entry_type' => GiftSelectType::class,
                    'entry_options' => array('label' => false),
                    'block_name' => 'gift_list_' . $list->getId()
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GiftList::class,
            'translation_domain' => 'form',
        ));
    }
}
