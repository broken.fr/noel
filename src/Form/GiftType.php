<?php

namespace App\Form;

use App\Entity\Gift;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GiftType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'gift.name',
                    'required' => true
                )
            )
            ->add(
                'description',
                TextType::class,
                array(
                    'label' => 'gift.description',
                    'required' => false
                )
            )
            ->add(
                'link',
                TextType::class,
                array(
                    'label' => 'gift.link',
                    'required' => false
                )
            )
            ->add(
                'linkBis',
                TextType::class,
                array(
                    'label' => 'gift.linkBis',
                    'required' => false
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Gift::class,
            'translation_domain' => 'form',
        ));
    }
}
