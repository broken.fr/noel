CHANGELOG
=========

2.x.x
-----
### Fixes
* Fix Bottom footer is overlapping with the content on chrome #20

2.1.2
-----

### Upgrades
- Fix title is too long on xs screens
- handle remember me option

2.1.0
-----

### Upgrades
- Graphical upgrade

2.0.0
-----

### Upgrades
- Upgrade symfony to Symfony 5.1

### Features
* Dockerize the project

### Updates
- Remove other year gifts from selected gift list
- Don't show the link if thee database field is empty
- Change links fields to 'text' instead of 'string'

### Removes
- Removes friendsofsymfony/userBundle integration

1.3.0
-----

### Features
* Add description field

### Updates
* Update surpriseGift to be usable by and for everyone (even user who have not created any list)
* Update title of the shown list with the owner of the list in addition

### Fixes 
* Fix position of surprise gift button

1.2.1
-----
### Fixes
* fix santa icon & icon_color default value in entity

1.2.0
-----
### Updates
* Update mailer, with smtp from ionos

1.1.0
-----
### Updates
* Update login form with forgot password page

### Fixes
* Fix entity fk user_id column in many to Many
* Fix forgot password page

1.0.1
-----
### Fixes
* Fix giftlist list to display only list of current year

1.0.0
-----
### Features
+ Add multiple santas to gift entity
+ Add unselect gift action
+ Add santa count on giftlist gift description
+ Add loader js

### Updates
* Add changlog file

### Fixes
* update css, fix some mobile css
