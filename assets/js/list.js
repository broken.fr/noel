const {noelLoader} = require("./loader/loader");

var route = Routing.generate('list_gift_select');
$('input[type="checkbox"]').on('change', function(){
    noelLoader.init();
    var form = $(this).closest('.form-ajax');
    var label = $(this).parent().find('label');
    var selected = label.hasClass('line-through') ? true : false;
    var form_data = {
        'gift': form.data('gift-id'),
        'list': form.data('list-id')
    };
    var parent = $(this).closest('li.form-ajax');
    var user_count = parent.attr('data-user-count');
    // console.log(user_count);
    // $(this).attr('disabled', true);
    if(selected){
        // remove santa from list
        label.removeClass('line-through');
        user_count--;
        if(user_count === 0){
            parent.find('.uc').remove();
        } else {
            parent.find('.uc').html('[' + user_count + ' santa(s)]');
        }
    } else {
        // add santa from list
        label.addClass('line-through');
        user_count++;
        if(user_count > 1){
            parent.find('.uc').html('[' + user_count + ' santa(s)]');
        } else {
            parent.find('label').append('<span class="green-text d-block uc">[' + user_count + ' santa(s)]</span>');
        }
    }
    parent.attr('data-user-count', user_count);
    $.ajax({
        url: route,
        type: 'POST',
        dataType: 'json',
        data: form_data,
        success:function(data){
            // handling the response data from the controller
            if(data.status == 'error'){
                console.error("[API] ERROR: " + data.message);
                $(this).attr('disabled', false);
                label.removeClass('line-through');
                toastr.error(data.message);
            }
            if(in_array(data.status, ['info', 'success', 'warning'])){
                console.log("[API] " + data.status + ": " + data.message);
                // Display an info toast with no title
                toastr[data.status](data.message);
            }
            noelLoader.unload();
        }
    });
});

function in_array(needle, haystack) {
    for(var i in haystack) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
