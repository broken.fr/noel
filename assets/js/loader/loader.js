module.exports = {
    noelLoader: {
        init: function (txt) {
            var template = '<div class="loader-wrapper">\n'
                + '<div class="preloader-wrapper big active">\n'
                + '<div class="spinner-layer spinner-green-only">\n'
                + '<div class="circle-clipper left"><div class="circle"></div></div>\n'
                + '<div class="gap-patch"><div class="circle"></div></div>\n'
                + '<div class="circle-clipper right"><div class="circle"></div></div>\n'
                + '</div>\n</div>\n</div>';
            document.body.insertAdjacentHTML('beforeend', template);
            setTimeout(function () { //to trigger fade in effect
                document.querySelector(".loader-wrapper").classList.add('fade');
            }, 1);
            // this.load();
        },
        unload: function () {
            var template = document.querySelector(".loader-wrapper");
            if (template !== null) {
                template.classList.remove('fade');
                setTimeout(function () { // let the time for fade outs
                    template.remove();
                }, 500);
            }
        }
    }
}
