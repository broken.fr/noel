$(document).ready(function () {
    var sidebar = $('.sidebar');
    var body = $('.wrapper');
    var sidebarOff = function() {
        sidebar.removeClass('active');
        body.removeClass('active');
        sidebar.find('.sidebar-trigger i').removeClass('fa-minus').addClass('fa-plus');
    };
    var sidebarOn = function() {
        sidebar.addClass('active');
        body.addClass('active');
        sidebar.find('.sidebar-trigger i').removeClass('fa-plus').addClass('fa-minus');
    };

    sidebar.on('click', '.sidebar-trigger', function() {
        if (sidebar.hasClass('active') && body.hasClass('active')) {
            sidebarOff();
            return false;
        }
        sidebarOn();
    });
    body.after().on('click', function() {
        if (sidebar.hasClass('active') && body.hasClass('active')) {
            sidebarOff();
            return false;
        }
    });

    $('.mobile-sidebar-trigger').on('click', function(){
        //if($(window).width() < 768){
            var open = $(this).find('.to-open'), close = $(this).find('.to-close');
            if (sidebar.hasClass('active') && body.hasClass('active')) {
                $(this).addClass('btn-primary').removeClass('btn-danger');
                close.addClass('hidden-md-down');
                open.removeClass('hidden-md-down');
                sidebarOff();
                return false;
            }
            $(this).addClass('btn-danger').removeClass('btn-primary');
            open.addClass('hidden-md-down');
            close.removeClass('hidden-md-down');
            sidebarOn();
        //}
    });

    /* ETATSFLASH SIDEBAR Scroll to anchor by jQuery */
    $('.etatsflash_shortcut li a').click(function (e) {
        e.preventDefault();
        sidebarOff();
        var anchor_id = $(this).attr('href');
        var scrollTo = $(anchor_id).offset().top - 60;
        console.log(scrollTo);
        $('.inner-wrapper').animate({scrollTop: scrollTo}, 'slow');
    });
});
