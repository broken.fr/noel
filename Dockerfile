FROM node:18 as build-step
#FROM node:latest as build-step
RUN ln -s /usr/bin/nodejs /usr/bin/node
RUN mkdir -p /app
WORKDIR /app
COPY package.json yarn.lock /app/
#RUN yarn install --frozen-lockfile --network-timeout 1000000
RUN npm install -timeout=1000000
COPY . /app
#RUN yarn build
RUN npm run build

FROM php:8.1-fpm as php-step
# RUN apk add --no-cache pcre-dev $PHPIZE_DEPS && pecl install redis docker-php-ext-enable redis.so
RUN pecl install redis && docker-php-ext-enable redis
RUN apt-get update && apt-get install -y libzip-dev zip && docker-php-ext-install zip
RUN docker-php-ext-configure zip
RUN docker-php-ext-install pdo pdo_mysql zip

# PHP-FPM defaults
ENV PHP_FPM_PM="dynamic"
ENV PHP_FPM_MAX_CHILDREN="10"
ENV PHP_FPM_START_SERVERS="4"
ENV PHP_FPM_MIN_SPARE_SERVERS="2"
ENV PHP_FPM_MAX_SPARE_SERVERS="4"
ENV PHP_FPM_MAX_REQUESTS="1000"

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

FROM php-step
# Copy the PHP-FPM configuration file
COPY ./www.conf /usr/local/etc/php-fpm.d/www.conf

#RUN adduser --disabled-password --gecos "" --ingroup "www-data" --home "/home/pyrowman" pyrowman
#RUN addgroup pyrowman root

COPY --from=build-step /app /var/www/public
WORKDIR /var/www/public
RUN cd /var/www/public

# Copy the PHP application file
RUN composer install --prefer-dist --no-dev
RUN chown -R www-data:www-data /var/www/public

#USER pyrowman
#CMD composer run-script auto-scripts && php bin/console do:mi:mi --no-interaction && php-fpm
